package br.com.dblogic.jadventuregdx.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import br.com.dblogic.jadventuregdx.JAdventureGDX;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "JAdventure GDX v.0.1";
		config.useGL30 = true;
		config.width = 800;
		config.height = 600;
		new LwjglApplication(new JAdventureGDX(), config);
	}
}
