# jAdventureGDX

Remake of [Atari´s Adventure](https://en.wikipedia.org/wiki/Adventure_(Atari_2600)) made with libgdx. 

## Running the game

It´s simple. After downloading, run:
```
gradle build run
```