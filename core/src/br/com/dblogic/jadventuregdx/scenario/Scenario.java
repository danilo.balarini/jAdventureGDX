package br.com.dblogic.jadventuregdx.scenario;

import com.badlogic.gdx.graphics.Color;

public class Scenario {

	public Scenario(int screenCode, int north, int east, int west, int south, Color cor) {

		this.setScreenCode(screenCode);
		this.setNorth(north);
		this.setEast(east);
		this.setWest(west);
		this.setSouth(south);
		this.setCor(cor);
		this.setScreen(screenCode);

	}

	private int screenCode;

	private int north;
	private int east;
	private int west;
	private int south;

	private Color cor;

	private StringBuffer screen;

	public int getScreenCode() {
		return screenCode;
	}

	public void setScreenCode(int code) {
		this.screenCode = code;
	}

	public int getNorth() {
		return north;
	}

	public void setNorth(int north) {
		this.north = north;
	}

	public int getEast() {
		return east;
	}

	public void setEast(int east) {
		this.east = east;
	}

	public int getWest() {
		return west;
	}

	public void setWest(int west) {
		this.west = west;
	}

	public int getSouth() {
		return south;
	}

	public void setSouth(int south) {
		this.south = south;
	}

	public Color getCor() {
		return cor;
	}

	public void setCor(Color cor) {
		this.cor = cor;
	}

	public StringBuffer getScreen() {
		return screen;
	}

	public void setScreen(int screenCode) {

		StringBuffer map = new StringBuffer();

		switch (screenCode) {

		case 0: {
			map.append("0000000000000000000000000000000000000000");
			map.append("0000000000000000000000000000000000000000");
			map.append("0000000000000000000000000000000000000000");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("0000000000000000        0000000000000000");
			map.append("0000000000000000        0000000000000000");
			break;
		}

		case 1: {
			map.append("00000000000 0 0 0      0 0 0 00000000000");
			map.append("00000000000 0 0 0      0 0 0 00000000000");
			map.append("00        0000000      0000000        00");
			map.append("00        0000000      0000000        00");
			map.append("00        0000000      0000000        00");
			map.append("00        0000000      0000000        00");
			map.append("00        00000000000000000000        00");
			map.append("00        00000000000000000000        00");
			map.append("00        00000000000000000000        00");
			map.append("00        00000000000000000000        00");
			map.append("00          0000000000000000          00");
			map.append("00          0000000000000000          00");
			map.append("00          0000000000000000          00");
			map.append("00          0000000000000000          00");
			map.append("00          0000000000000000          00");
			map.append("00          000000 FA 000000          00");
			map.append("00          000000 FA 000000          00");
			map.append("00          000000 FA 000000          00");
			map.append("00          000000 FA 000000          00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("00                                    00");
			map.append("0000000000000000        0000000000000000");
			map.append("0000000000000000        0000000000000000");
			break;
		}

		case 2: {
			map.append("0000000000000000        0000000000000000");
			map.append("0000000000000000        0000000000000000");
			map.append("0000000000000000        0000000000000000");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("                                        ");
			map.append("0000000000000000000000000000000000000000");
			map.append("0000000000000000000000000000000000000000");
			map.append("0000000000000000000000000000000000000000");
			break;
		}

		}

		this.screen = map;

	}

}