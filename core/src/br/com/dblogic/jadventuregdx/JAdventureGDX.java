package br.com.dblogic.jadventuregdx;

import java.util.ArrayList;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import br.com.dblogic.jadventuregdx.scenario.Scenario;

public class JAdventureGDX extends ApplicationAdapter {

	private ShapeRenderer player;

	private float speed = 4.8f;

	private float playerstartx = 300f;
	private float playerstarty = 300f;
	private float squareheight = 24f;
	private float squarewidth = 20f;

	private SpriteBatch batch;
	private BitmapFont text;

	private OrthographicCamera cam;

	private ArrayList<Scenario> scenarios;
	private Pixmap pixmap;
	private Texture screen;
	
	private Sprite spriteScreen;

	@Override
	public void create() {

		batch = new SpriteBatch();

		initScenarios();

		// font
		text = new BitmapFont();
		text.setColor(Color.GREEN);
		text.getData().setScale(2f);

		// player
		player = new ShapeRenderer();
		player.setColor(Color.ORANGE);
		player.setAutoShapeType(true);

		cam = new OrthographicCamera();
		cam.setToOrtho(false, 800, 600);
		
		screen = new Texture(imageScreen(this.scenarios.get(1)));
		screen.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		
		spriteScreen = new Sprite(screen);
		spriteScreen.setPosition(0, 0);
		spriteScreen.setSize(800, 600);
		
	}

	@Override
	public void render() {

		handleInput();
		batch.setProjectionMatrix(cam.combined);

		Gdx.gl.glClearColor(0.3f, 0.4f, 0.8f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.begin();
		spriteScreen.draw(batch);
		batch.end();

		batch.begin();
		text.draw(batch, "fps: " + Gdx.graphics.getFramesPerSecond(), 700, 50);
		batch.end();

		batch.begin();
		player.begin();

		player.set(ShapeType.Filled);
		player.rect(playerstartx, playerstarty, squarewidth, squareheight);

		player.end();
		batch.end();
	}

	@Override
	public void dispose() {
		player.dispose();
		batch.dispose();
		text.dispose();
		screen.dispose();
		pixmap.dispose();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	private void handleInput() {
		if (Gdx.input.isKeyPressed(Keys.UP)) playerstarty += speed;
		if (Gdx.input.isKeyPressed(Keys.DOWN)) playerstarty -= speed;
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) playerstartx += speed;
		if (Gdx.input.isKeyPressed(Keys.LEFT)) playerstartx -= speed;
	}

	private void initScenarios() {
		this.scenarios = new ArrayList<Scenario>(25);
		this.scenarios.add(new Scenario(0, 1, 1, 1, 1, Color.ORANGE));
		this.scenarios.add(new Scenario(1, 0, 2, 2, 2, Color.ORANGE));
		this.scenarios.add(new Scenario(2, 1, 3, 3, 3, Color.GREEN));
	}

	public Pixmap imageScreen(Scenario scenario) {

		pixmap = new Pixmap(40, 25, Format.RGBA8888);
		pixmap.setColor(Color.WHITE);
		pixmap.fill();

		int x = 0, y = 0;
		char arr[] = scenario.getScreen().toString().toCharArray();
		pixmap.setColor(Color.GOLD);
		for(char c : arr) {
			
			// Gdx.app.log("ISBRICK: ", "char: " + c + " isBrick: " + isBrick(c));
			Gdx.app.log("DRAWPIXEL: ", "pixmap.drawPixel(x: " + x + " , y: " + y + "); isBrick: " + isBrick(c) + " char: " + c + " isBrick: " + isBrick(c));
			
			if(isBrick(c)) {
				//Gdx.app.log("DRAWPIXEL: ", "pixmap.drawPixel(x: " + x + " , y: " + y + "); isBrick: " + isBrick(c) + " char: " + c + " isBrick: " + isBrick(c));
				pixmap.drawPixel(x, y);
			}
			
			if(x == 40) { 
				x = 0;
				y++;
			} 
			
			x++;
		}
					
		return pixmap;
	}
	
	private boolean isBrick(char letra) {
		return (letra != ' ') ? true : false;
	}

}